@extends('layouts.app')

@php
  $active_page = "Ranks";
@endphp


@section('title', 'Ranks')

@section('content')
<?php 

$achievement = 0; 

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($selected_period == null)
                        <div class="alert alert-danger" role="alert">
                            There are no period in this day
                        </div>
                    @else
                    <form method="POST" action="{{ route("ranks.filterByPeriod") }}">
                      @csrf
                      <div class="row">
                        <div class="col col-8">
                          <div class="form-group">
                            <label>Select Period</label>
                            <select class="form-control" name="period_id">
                              @foreach($periods as $period)
                                <option value="{{ $period->id }}" {{ $selected_period->id == $period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col col-4">
                          <div class="form-group">
                            <label class="text-white">Button</label>
                            <button type="submit" class="btn btn-success btn-block">Go</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    @endif
                    
                    @if(count($volunteers) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Volunteer Name</th>
                                <th>Achievement</th>
                                <th>Transaction</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Volunteer Name</th>
                                <th>Achievement</th>
                                <th>Transaction</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($volunteers as $key => $volunteers)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td class="{{ Auth::user()->name == $volunteers->name ? 'font-weight-bold' : ''}}">{{ $volunteers->name }}</td>
                                <td class="text-right">{{ rupiah($volunteers->achievements) }}</td>
                                <td class="text-center">{{ $volunteers->transactions }}</td>
                            </tr>
                            
                            @endforeach
                            
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
