@extends('layouts.app')

@php
  $active_page = "Resume";
@endphp


@section('title', 'Resume')

@section('content')
<?php 

$achievement = 0; 

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if($selected_period == null)
                        <div class="alert alert-danger" role="alert">
                            There are no period in this day
                        </div>
                    @else
                    <form method="POST">
                      @csrf
                      <div class="row">
                        <div class="col col-8">
                          <div class="form-group">
                            <label>Select Period</label>
                            <select class="form-control" name="period_id">
                              @foreach($periods as $period)
                                <option value="{{ $period->id }}" {{ $selected_period->id == $period->id ? 'selected' : '' }}>{{ $period->name }}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="col col-2">
                          <div class="form-group">
                            <label class="text-white">Button</label>
                            <button formaction="{{ route("resume.filterByPeriod") }}" class="btn btn-success btn-block">Go</button>
                          </div>
                        </div>
                        <div class="col col-2">
                          <div class="form-group">
                            <label class="text-white">Button</label>
                            <button formaction="{{ route("resume.export") }}" class="btn btn-danger btn-block">Export</button>
                          </div>
                        </div>
                      </div>
                    </form>
                    @endif
                    
                    @if(count($donations) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Donation Type</th>
                                <th>Income</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Donation Type</th>
                                <th>Income</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($donations as $key => $donation)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $donation->name }}</td>
                                <td class="text-right">{{ rupiah($donation->income) }}</td>
                            </tr>
                            <?php 
                              $achievement += $donation->income;
                            ?>
                            @endforeach
                            
                        </tbody>
                    </table>
                    @endif
                    <h3>Total Achievement: {{ rupiah($achievement) }}</h3>

                    @if($target > 0)
                    <h3>Target: {{ rupiah($target) }}</h3>
                    <div class="progress">
                      <div class="progress-bar" role="progressbar" style="width: {{ round($achievement/$target*100,2) }}%;" aria-valuenow="{{ round($achievement/$target*100,2) }}" aria-valuemin="0" aria-valuemax="100">{{ round($achievement/$target*100,2) }}%</div>
                    </div>
                    <h3>Progress: {{ round($achievement/$target*100,2) }}%</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
