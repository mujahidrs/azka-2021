<!-- Main Navbar-->
<header class="header">
    <nav class="navbar">
    <!-- Search Box-->
    <div class="search-box">
        <button class="dismiss"><i class="icon-close"></i></button>
        <form id="searchForm" action="#" role="search">
        <input type="search" placeholder="What are you looking for..." class="form-control">
        </form>
    </div>
    <div class="container-fluid">
        <div class="navbar-holder d-flex align-items-center justify-content-between">
        <!-- Navbar Header-->
        <div class="navbar-header">
            <!-- Toggle Button--><a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
            <!-- Navbar Brand -->
            <a class="navbar-brand" href="{{ url('/') }}" class="navbar-brand d-none d-sm-inline-block">
                <div class="brand-text d-none d-lg-inline-block"><strong>{{ config('app.name', 'Laravel') }}</strong></div>
                <div class="brand-text d-none d-sm-inline-block d-lg-none"><strong><strong>AZ</strong></div>
            </a>
            
        </div>
        <!-- Navbar Menu -->
        <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
            <!-- Authentication Links -->
            @guest
            @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
            @endif
            
            @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
            @endif
            @else
            <!-- Logout    -->
            <li class="nav-item">
                <a href="{{ route('logout') }}" class="nav-link logout" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <span class="d-none d-sm-inline">Logout</span><i class="fa fa-sign-out"></i>
                </a>
                
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>    
            </li>
        @endguest
        </ul>
        </div>
    </div>
    </nav>
</header>