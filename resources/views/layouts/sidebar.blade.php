<!-- Side Navbar -->
<nav class="side-navbar">
    <!-- Sidebar Header-->
    <div class="sidebar-header d-flex align-items-center">
        <div class="avatar">
            @if(Auth::user()->gender === 'male')
                <img src="{{ asset('img/avatar-ikhwan.png') }}" alt="..." class="img-fluid rounded-circle">
            @elseif(Auth::user()->gender === 'female')
                <img src="{{ asset('img/avatar-akhwat.png') }}" alt="..." class="img-fluid rounded-circle">
            @endif
        </div>

        <div class="title">
            <h1 class="h4">{{ Auth::user()->name }}</h1>
        </div>
    </div>

    <!-- Sidebar Navidation Menus-->
    <div>        
        <span class="heading">Main</span>
        <ul class="list-unstyled">
            <li class="{{ $active_page === "Dashboard" ? 'active' : '' }}">
                <a href="{{ route("home") }}"><i class="fa fa-tachometer"></i> Dashboard</a>
            </li>
            <li class="{{ $active_page === "Profile" ? 'active' : '' }}">
                <a href="{{ route("profile") }}"><i class="fa fa-user"></i> Profile</a>
            </li>
        </ul>
        @if(Auth::user()->role_id === 1)
        <span class="heading">Super Admin</span>
            <ul class="list-unstyled">
                <li class="{{ $active_page === "Periods" ? 'active' : '' }}">
                    <a href="{{ route("superadmin.periods") }}"><i class="fa fa-clock-o"></i> Periods</a>
                </li>
                <li class="{{ $active_page === "Donations" ? 'active' : '' }}">
                    <a href="{{ route("superadmin.donations") }}"><i class="fa fa-bar-chart"></i> Donations</a>
                </li>
                <li class="{{ $active_page === "Levels" ? 'active' : '' }}">
                    <a href="{{ route("superadmin.levels") }}"><i class="fa fa-sitemap"></i> Levels</a>
                </li>
                <li class="{{ $active_page === "Classes" ? 'active' : '' }}">
                    <a href="{{ route("superadmin.classes") }}"><i class="fa fa-object-group"></i> Classes</a>
                </li>
                <li class="{{ $active_page === "Users" ? 'active' : '' }}">
                    <a href="{{ route("superadmin.users") }}"><i class="fa fa-users"></i> Users</a>
                </li>
            </ul>
            @endif
            @if(Auth::user()->role_id === 2)
            <span class="heading">Admin</span>
            <ul class="list-unstyled">
                <li class="{{ $active_page === "Volunteers" ? 'active' : '' }}">
                    <a href="{{ route("admin.volunteers") }}"><i class="fa fa-users"></i> Volunteers</a>
                </li>
                <li class="{{ $active_page === "Recaps" ? 'active' : '' }}">
                    <a href="{{ route("admin.recaps") }}"><i class="fa fa-history"></i> Recaps</a>
                </li>
                <li class="{{ $active_page === "Resume" ? 'active' : '' }}">
                    <a href="{{ route("resume") }}"><i class="fa fa-address-book"></i> Resume</a>
                </li>
                <li class="{{ $active_page === "Ranks" ? 'active' : '' }}">
                    <a href="{{ route("ranks") }}"><i class="fa fa-trophy"></i> Ranks</a>
                </li>
                <li class="{{ $active_page === "Media" ? 'active' : '' }}">
                    <a href="{{ route("media") }}"><i class="fa fa-file"></i> Media</a>
                </li>
            </ul>
            @endif
            @if(Auth::user()->role_id === 3)
            <span class="heading">Volunteer</span>
            <ul class="list-unstyled">
                <li class="{{ $active_page === "Reports" ? 'active' : '' }}">
                    <a href="{{ route("volunteer.reports") }}"><i class="fa fa-bar-chart"></i> Reports</a>
                </li>
                <li class="{{ $active_page === "Resume" ? 'active' : '' }}">
                    <a href="{{ route("resume") }}"><i class="fa fa-address-book"></i> Resume</a>
                </li>
                <li class="{{ $active_page === "Ranks" ? 'active' : '' }}">
                    <a href="{{ route("ranks") }}"><i class="fa fa-trophy"></i> Ranks</a>
                </li>
                <li class="{{ $active_page === "Certificate" ? 'active' : '' }}">
                    <a href="{{ route("certificate") }}"><i class="fa fa-certificate"></i> Certificate</a>
                </li>
                <li class="{{ $active_page === "Media" ? 'active' : '' }}">
                    <a href="{{ route("media") }}"><i class="fa fa-file"></i> Media</a>
                </li>
                <li class="{{ $active_page === "Calculator" ? 'active' : '' }}">
                    <a href="{{ route("calculator") }}"><i class="fa fa-calculator"></i>Calculator</a>
                </li>
            </ul>
        @endif
    </div>
</nav>
