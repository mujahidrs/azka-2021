@extends('layouts.app')

@php
  $active_page = "Classes";
@endphp

@section('title', 'Classes')

@section('content')
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md mb-2">
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addData">
          Add Class
        </button>
        <!-- Modal -->
        <div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Class</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form method="POST" action="{{ route("superadmin.classes.store") }}">
              @csrf
                  <div class="modal-body">
                    <div class="form-group">
                        <label>Class Code</label>
                        <input type="text" name="code" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Level</label>
                        <select class="form-control" name="level_id">
                          @foreach($levels as $key => $level)
                            <option value="{{ $level->id }}">{{ $level->id }}</option>
                          @endforeach
                        </select>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    @if(count($classes) === 0)
                        <div class="alert alert-danger" role="alert">
                            There are no data in this table
                        </div>
                    @else
                    <table id="example1" class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Class Code</th>
                                <th>Level</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Class Code</th>
                                <th>Level</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($classes as $key => $class)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $class->code }}</td>
                                <td>{{ $class->level_id }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editData-{{ $class->id }}"><i class="fa fa-edit"></i></button>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteData-{{ $class->id }}"><i class="fa fa-trash"></i></button>
                                    </div>

                                </td>
                            </tr>
                            
                            <!-- Modal -->
                            <div class="modal fade" id="editData-{{ $class->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Class</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.classes.update", ["id"=>$class->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        <div class="form-group">
                                            <label>Class Code</label>
                                            <input type="text" name="code" class="form-control" value="{{ $class->code }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Level</label>
                                            <select class="form-control" name="level_id">
                                              @foreach($levels as $key => $level)
                                                <option value="{{ $level->id }}" {{ $level->id == $class->level_id ? 'selected' : '' }}>{{ $level->id }}</option>
                                              @endforeach
                                            </select>
                                        </div>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="deleteData-{{ $class->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Delete Class</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="POST" action="{{ route("superadmin.classes.delete", ["id"=>$class->id]) }}">
                                  @csrf
                                      <div class="modal-body">
                                        Are you sure you want to delete this data?
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                        <button type="submit" class="btn btn-primary">Yes</button>
                                      </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            @endforeach
                        </tbody>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
