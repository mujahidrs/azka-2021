@extends('layouts.app')

@php
  $active_page = "Profile";
@endphp

@section('title', 'Profile')

@section('content')
<div class="container">
  @if (session('status'))
      <div class="alert alert-success" role="alert">
          {{ session('status') }}
      </div>
  @endif
  <div class="card">
    <div class="card-header">
      <div class="card-title">
        Profile
      </div>
      @if(Auth::user()->is_confirmed === 0)
      <div class="alert alert-warning">
        Please confirm your profile especially your Partnership Code, is correct?
      </div>
      @endif
    </div>
    <div class="card-body">
      <table class="table" id="example2">
        <thead>
          <tr>
            <th class="text-center">Field</th>
            <th class="text-center">Value</th>
            <th class="text-center">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>Name</th>
            <td>{{ $user->name }}</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#name">
                Change
              </button>
            </td>
          </tr>
          <tr>
            <th>Gender</th>
            <td>{{ $user->gender === 'male' ? 'Male' : ($user->gender === 'female' ? 'Female' : null) }}</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#gender">
                Change
              </button>
            </td>
          </tr>
          <tr>
            <th>Email</th>
            <td>{{ $user->email }}</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#email">
                Change
              </button>
            </td>
          </tr>
          <tr>
            <th>Phone</th>
            <td>{{ $user->phone }}</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#phone">
                Change
              </button>
            </td>
          </tr>
          <tr>
            <th>Username</th>
            <td>{{ $user->username }}</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#username">
                Change
              </button>
            </td>
          </tr>
          <tr>
            <th>Password</th>
            <td>(secret)</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#password">
                Change
              </button>
            </td>
          </tr>
          <tr>
            <th>Partnership Code</th>
            <td>{{ $user->class?->code }}</td>
            <td class="text-center">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#partnership_code">
                Change
              </button>
            </td>
          </tr>
        </tbody>
      </table>
      @if(Auth::user()->is_confirmed === 0)
      <form method="POST" action="{{ route('profile.is_confirmed.update') }}">
          @csrf
        <button class="btn btn-primary" type="submit">Yes, that's right</button>
      </form>
      @endif

      <!-- Modal -->
      <div class="modal fade" id="name" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("profile.name.update") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Name</label>
                        <input type="text" disabled class="form-control" value="{{ $user->name }}">
                    </div>
                    <div class="form-group">
                        <label>New Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="gender" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Gender</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("profile.gender.update") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Gender</label>
                        <input type="text" disabled class="form-control" value="{{ $user->gender === 'male' ? 'Male' : ($user->gender === 'female' ? 'Female' : null) }}">
                    </div>
                    <div class="form-group">
                        <label>New Gender</label>
                        <select class="form-control" name="gender">
                          <option selected disabled>-- Select Option --</option>
                          <option value="male">Male</option>
                          <option value="female">Female</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="partnership_code" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Partnership Code</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("profile.partnership_code.update") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Partnership Code</label>
                        <input type="text" disabled class="form-control" value="{{ $user->class?->code }}">
                    </div>
                    <div class="form-group">
                        <label>New Partnership Code</label>
                        {{-- <input type="text" name="partnership_code" class="form-control"> --}}
                        <select class="form-control" name="class_id">
                          <option disabled selected>-- Select Option --</option>
                          @foreach ($classes as $class)
                            <option value="{{ $class->id }}">{{ $class->code }}</option>
                          @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="username" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Username</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("profile.username.update") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Username</label>
                        <input type="text" disabled class="form-control" value="{{ $user->username }}">
                    </div>
                    <div class="form-group">
                        <label>New Username</label>
                        <input type="text" name="username" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="email" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Email</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("changeEmail") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Email</label>
                        <input type="text" disabled class="form-control" value="{{ $user->email }}">
                    </div>
                    <div class="form-group">
                        <label>New Email</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="phone" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Phone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("profile.phone.update") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Old Phone</label>
                        <input type="text" disabled class="form-control" value="{{ $user->phone }}">
                    </div>
                    <div class="form-group">
                        <label>New Phone</label>
                        +62
                        <input type="number" name="phone" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

      <!-- Modal -->
      <div class="modal fade" id="password" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route("profile.password.update") }}">
            @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
        </div>
      </div>

    </div>
  </div>
</div>
@endsection
