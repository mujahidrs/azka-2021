@extends('layouts.app')

@php
  $active_page = "Recaps";
  $active_page2 = "Reports";
@endphp

@section('title', 'Show Report')

@section('content')
<?php

function rupiah($angka){
  
  $hasil_rupiah = "Rp " . number_format($angka,2,',','.');
  return $hasil_rupiah;
 
}

?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <a href="{{ route("volunteer.reports") }}" class="btn btn-primary float-left">
                              Back
                            </a>
                        </div>
                        <div class="col text-center">
                            <h3>Report Detail</h3>
                        </div>
                        <div class="col">
                        </div>
                    </div>
                </div>
                <div class="card-body">
                  <table class="table">
                    <tr>
                      <th>Invoice</th>
                      <td>{{ $reports[0]->invoice }}</td>
                    </tr>
                    <tr>
                      <th>Volunteer Name</th>
                      <td>{{ $reports[0]->volunteer->name }}</td>
                    </tr>
                    <tr>
                      <th>Donor Name</th>
                      <td>{{ $reports[0]->donor->name }}</td>
                    </tr>
                    <tr>
                      <th>Donor Address</th>
                      <td>{{ $reports[0]->donor->address }}</td>
                    </tr>
                    <tr>
                      <th>Donor Phone</th>
                      <td>{{ $reports[0]->donor->phone }}</td>
                    </tr>
                    <tr>
                      <th>Donor Email</th>
                      <td>{{ $reports[0]->donor->email }}</td>
                    </tr>
                    <tr>
                      <th>Donor NPWP</th>
                      <td>{{ $reports[0]->donor->npwp }}</td>
                    </tr>
                    <tr>
                      <th>Donation Date</th>
                      <td>{{ $reports[0]->date }}</td>
                    </tr>
                    <tr>
                      <th>Donation Type</th>
                      <td>{{ $reports[0]->type == 'transfer' ? 'Transfer' : 'Cash' }}</td>
                    </tr>
                    <tr>
                      <th>Donation To</th>
                      <td>
                        @if($reports[0]->donation_to === 'virtual')
                          Virtual Account
                        @elseif($reports[0]->donation_to === 'bank')
                          Bank Account
                        @else
                          Cash
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <th>Proof of Payment</th>
                      <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#showImage"><i class="fa fa-eye"></i></button>

                        <!-- Modal -->
                        <div class="modal fade" id="showImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Proof of Payment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body text-center">
                                <img src="{{ $reports[0]->proof }}" class="img-thumbnail"></td>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                    </tr>
                  </table>
                  <table class="table" id="example2">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Donation Name</th>
                        <th>Nominal</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                        $i = 0;
                        $sum_nominal = 0;
                      ?>
                      @foreach($reports as $key => $report)
                        @if($report->nominal > 0)
                        <tr>
                          <td>{{ $i+1 }}</td>
                          <td>{{ $report->donation->name }}</td>
                          <td>
                            {{ rupiah($report->nominal) }}
                            <?php $sum_nominal+=$report->nominal ?>
                          </td>
                          <td>{{ $report->desc }}</td>
                        </tr>
                        <?php $i++ ?>
                        @endif
                      @endforeach
                    </tbody>
                  </table>

                  <h4>Total Achievement: {{ rupiah($sum_nominal) }}</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
