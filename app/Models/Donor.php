<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address',
        'phone',
        'npwp',
        'email',
    ];

    public function reports()
    {
    	return $this->belongsTo(Report::class);
    }
}
