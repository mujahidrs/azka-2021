<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distribution extends Model
{
    use HasFactory;

    protected $fillable = [
        'invoice',
        'treasurer_id',
        'donor_id',
        'donation_id',
        'nominal',
        'period_id',
        'date',
        'type',
        'proof',
        'desc',
    ];
}
