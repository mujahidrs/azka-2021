<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Target extends Model
{
    use HasFactory;

    protected $fillable = [
    	'period_id',
    	'level_id',
    	'target',
    ];

    public function period()
    {
    	return $this->hasOne(Period::class);
    }

    public function level()
    {
    	return $this->hasOne(Level::class);
    }
}
