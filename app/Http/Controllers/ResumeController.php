<?php

namespace App\Http\Controllers;

use App\Exports\ResumeExport;
use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Period;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Target;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ResumeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = date('Y-m-d H:i');

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        $donations = Donation::where('period_id', $selected_period->id)->orderBy('sort_number', 'asc')->get();

        $target = $selected_period->total_target;

        foreach($donations as $key => $donation) {
            $donation->income = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                                ->where('period_id', $selected_period->id)
                                ->where('donation_id', $donation->id)
                                ->groupBy('donation_id')
                                ->pluck('sum_nominal')
                                ->first();
        };

        return view('resume', [
            'periods' => $periods,
            'selected_period' => $selected_period,
            'target' => $target,
            'donations' => $donations,
        ]);
    }

    public function filterByPeriod(Request $request)
    {
        $periods = Period::all();

        $selected_period = Period::find($request->period_id);

        $donations = Donation::where('period_id', $selected_period->id)->orderBy('sort_number', 'asc')->get();

        $target = $selected_period->total_target;

        foreach($donations as $key => $donation) {
            $donation->income = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                                ->where('period_id', $selected_period->id)
                                ->where('donation_id', $donation->id)
                                ->groupBy('donation_id')
                                ->pluck('sum_nominal')
                                ->first();
        };

        return view('resume', [
            'periods' => $periods,
            'selected_period' => $selected_period,
            'target' => $target,
            'donations' => $donations,
        ]);
    }

    public function export(Request $request) 
    {
        $export_date = date("d-m-Y h:i A");
        $selected_period = $request->period_id;
        $donations = Donation::where('period_id', $selected_period)->orderBy('sort_number', 'asc')->get();

        $achievements = 0;

        foreach($donations as $key => $donation) {
            $donation->income = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                            ->where('period_id', $selected_period)
                            ->where('donation_id', $donation->id)
                            ->groupBy('donation_id')
                            ->pluck('sum_nominal')
                            ->first();
            
            $achievements += (int) $donation->income;
        };

        $period = Period::find($selected_period);

        if($period->total_target > 0){
            $percentage = $achievements / $period->total_target * 100;
        }
        else{
            $percentage = 0;
        }

        $filename = "Resume_" . str_replace('/', 'or', $period->name) . "_" . $export_date . ".xlsx";

        return (new ResumeExport($selected_period, $export_date, $donations, $achievements, $percentage))->download($filename);
    }
}
