<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;

class ReceiptController extends Controller
{
    public function index($id)
    {
    	$reports = Report::where('invoice', $id)->where("nominal", ">", 0)->get();

        return view('check_receipt')->with('reports', $reports);
    }
}
