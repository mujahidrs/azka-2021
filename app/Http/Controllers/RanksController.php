<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Report;
use App\Models\Period;
use App\Models\Donation;
use App\Models\Donor;
use App\Models\Target;
use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class RanksController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = date('Y-m-d H:i');

        $volunteers = User::where('role_id', 3)->get();

        $periods = Period::all();

        $selected_period = Period::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        foreach($volunteers as $key => $volunteer) {
            $volunteer->achievements = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                                        ->where('period_id', $selected_period->id)
                                        ->where('volunteer_id', $volunteer->id)
                                        ->groupBy('volunteer_id')
                                        ->orderBy('sum_nominal', 'ASC')
                                        ->pluck('sum_nominal')
                                        ->first();

            $volunteer->transactions = Report::where('nominal', '>', 0)
                                        ->where('period_id', $selected_period->id)
                                        ->where('volunteer_id', $volunteer->id)
                                        ->count();
        };

        return view('ranks', [
            'periods' => $periods,
            'selected_period' => $selected_period,
            'volunteers' => $volunteers->sortBy([['achievements', 'desc'], ['transactions', 'desc'], ['name', 'asc']])->values(),
        ]);
    }

    public function filterByPeriod(Request $request)
    {
        $volunteers = User::where('role_id', 3)->get();

        $periods = Period::all();

        $selected_period = Period::find($request->period_id);

        foreach($volunteers as $key => $volunteer) {
            $volunteers[$key]->achievements = Report::select(DB::raw('*, SUM(nominal) as sum_nominal'))
                                        ->where('period_id', $selected_period->id)
                                        ->where('volunteer_id', $volunteer->id)
                                        ->groupBy('volunteer_id')
                                        ->orderBy('sum_nominal', 'ASC')
                                        ->pluck('sum_nominal')
                                        ->first();

            $volunteers[$key]->transactions = Report::where('nominal', '>', 0)
                                        ->where('period_id', $selected_period->id)
                                        ->where('volunteer_id', $volunteer->id)
                                        ->count();
        };

        return view('ranks', [
            'periods' => $periods,
            'selected_period' => $selected_period,
            'volunteers' => $volunteers->sortBy([['achievements', 'desc'], ['transactions', 'desc'], ['name', 'asc']])->values(),
        ]);
    }
}
