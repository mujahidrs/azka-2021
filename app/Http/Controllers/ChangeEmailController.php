<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class ChangeEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->email !== null){
            $email = $request->email;

            $user = User::find(Auth::user()->id);

            $user->update([
                "email"=>$email,
                "email_verified_at"=>null
            ]);

            $user->sendEmailVerificationNotification();

            return redirect()->back()->with("status", "Change Email Success");
        }

        return redirect()->back()->with("status", "Change Email Failed");
    }
}
