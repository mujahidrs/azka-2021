<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Media;
use Illuminate\Support\Facades\Auth;

class MediaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media = Media::all();

        $months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        foreach ($media as $key => $item) {
            $item->tanggal = date("d", strtotime($item->created_at));
            $item->bulan = date("m", strtotime($item->created_at));
            $item->tahun = date("Y", strtotime($item->created_at));
            $item->waktu = date("H:i:s", strtotime($item->created_at));

            $item->bulan = $months[(int) $item->bulan];

            $item->date = $item->tanggal . ' ' . $item->bulan . ' ' . $item->tahun . ' ' . $item->waktu;
        }

        return view('media', [
            'media' => $media->sortByDesc("created_at")->values()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->title !== null && $request->file !== null){
            $title = $request->title;
            $desc = $request->desc;
            $path = null;
            $admin_id = $request->admin_id;

            if(isset($request->file)){
                $pathName = pathinfo($request->file->getClientOriginalName())['filename'];
                $pathExtension = $request->file->getClientOriginalExtension();
                $path = '/uploads/media/' . "media-" . $title . '-' . date("YmdHis") . '.' . $pathExtension;
            }
            
            $media = Media::create([
                "title" => $title,
                "desc" => $desc,
                "file" => $path,
                "admin_id" => $admin_id,
            ]);

            if(isset($request->file)){
                $request->file->move(public_path() . '/uploads/media/', $path);
            }

            return redirect()->back()->with("status", "Add Media Success");
        }
        return redirect()->back()->with("status", "Add Media Failed");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function download($id)
    {
        $media = Media::find($id);
        $file = public_path() . $media->file;

        return response()->download($file);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->title !== null){
            $title = $request->title;
            $desc = $request->desc;
            $path = null;

            if(isset($request->file)){
                $pathName = pathinfo($request->file->getClientOriginalName())['filename'];
                $pathExtension = $request->file->getClientOriginalExtension();
                $path = '/uploads/media/' . "media-" . $title . '-' . date("YmdHis") . '.' . $pathExtension;

                $media = Media::find($id)->update([
                    "title" => $title,
                    "desc" => $desc,
                    "file" => $path,
                    "admin_id" => Auth::user()->id,
                ]);

                $request->file->move(public_path() . '/uploads/media/', $path);
            }
            else{
                $media = Media::find($id)->update([
                    "title" => $title,
                    "desc" => $desc,
                    "admin_id" => Auth::user()->id,
                ]);
            }

            return redirect()->back()->with("status", "Update Media Success");
        }
        return redirect()->back()->with("status", "Update Media Failed");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Media::find($id)->delete();

        return redirect()->back()->with("status", "Delete Media Success");
    }
}
