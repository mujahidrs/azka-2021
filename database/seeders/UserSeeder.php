<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Custom\readLargeCSV;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('seeders/csv/users.csv');
	    $csv_reader = new readLargeCSV($file, ";");

	    $cur_time = date("Y-m-d H:i:s");

	    foreach($csv_reader->csvToArray() as $data){
	        // Preprocessing of the array.
	        foreach($data as $key => $entry){
	            // Laravel doesn't add timestamps on its own when inserting in chunks.
	            $data[$key]['created_at'] = $cur_time;
	            $data[$key]['updated_at'] = $cur_time;

	            if($data[$key]["email_verified_at"] === ""){
	            	$data[$key]["email_verified_at"] = null;
	            }
	            if($data[$key]["remember_token"] === ""){
	            	$data[$key]["remember_token"] = null;
	            }
	            if($data[$key]["phone"] === ""){
	            	$data[$key]["phone"] = null;
	            }
	        }
	        
	        User::insert($data);
	    }
    }
}
