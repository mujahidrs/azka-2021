<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class PasswordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = User::all();

    	foreach ($users as $key => $user) {
    		if($user->id >= 5){
    			$user->password = Hash::make($user->password);
    			
	    		User::find($user->id)->update(["password" => $user->password]);
    		}
    	}
    }
}
