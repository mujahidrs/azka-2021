<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Donation;
use App\Models\Role;
use App\Models\Level;
use App\Models\Classes;
use App\Models\Period;
use App\Models\Target;
use Illuminate\Support\Facades\Hash;

class FirstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Level::create(["id"=>1, "name"=>"1"]);
    	Level::create(["id"=>2, "name"=>"2"]);
    	Level::create(["id"=>3, "name"=>"3"]);
    	Level::create(["id"=>4, "name"=>"4"]);
    	Level::create(["id"=>5, "name"=>"5"]);

    	Classes::create(["code"=>"3101","level_id"=>3]);
    	Classes::create(["code"=>"3102","level_id"=>3]);
    	Classes::create(["code"=>"3103","level_id"=>3]);
    	Classes::create(["code"=>"3104","level_id"=>3]);
    	Classes::create(["code"=>"3105","level_id"=>3]);
    	Classes::create(["code"=>"3106","level_id"=>3]);
    	Classes::create(["code"=>"3107","level_id"=>3]);
    	Classes::create(["code"=>"3108","level_id"=>3]);
    	Classes::create(["code"=>"3109","level_id"=>3]);
    	Classes::create(["code"=>"3201","level_id"=>3]);
    	Classes::create(["code"=>"3202","level_id"=>3]);
    	Classes::create(["code"=>"3203","level_id"=>3]);
    	Classes::create(["code"=>"3204","level_id"=>3]);
    	Classes::create(["code"=>"3205","level_id"=>3]);
    	Classes::create(["code"=>"3206","level_id"=>3]);
    	Classes::create(["code"=>"3207","level_id"=>3]);
    	Classes::create(["code"=>"3208","level_id"=>3]);
    	Classes::create(["code"=>"3209","level_id"=>3]);
    	Classes::create(["code"=>"3210","level_id"=>3]);
    	Classes::create(["code"=>"3211","level_id"=>3]);
    	Classes::create(["code"=>"4101","level_id"=>4]);
    	Classes::create(["code"=>"4201","level_id"=>4]);
    	Classes::create(["code"=>"5000","level_id"=>5]);

        $super_admin_role = Role::create(["name"=>"Super Admin"]);

        $super_admin = User::create([
			"name"=>"Super Admin", 
			"email"=>"superadmin@gmail.com", 
			"username"=>"superadmin", 
			"password"=>Hash::make("ybpmmd123"),
			"role_id"=>$super_admin_role->id,
			"email_verified_at"=>date("Y-m-d H:i:s"),
			"gender"=>"male",
			"phone"=>null,
			"class_id"=>null,
		]);

		$admin_role = Role::create(["name"=>"Admin"]);

        $admin = User::create([
			"name"=>"Admin", 
			"email"=>"admin@gmail.com", 
			"username"=>"admin", 
			"password"=>Hash::make("ybpmmd123"),
			"role_id"=>$admin_role->id,
			"email_verified_at"=>date("Y-m-d H:i:s"),
			"gender"=>"male",
			"phone"=>null,
			"class_id"=>null,
		]);

		$volunteer_role = Role::create(["name"=>"Volunteer"]);

        $volunteer = User::create([
			"name"=>"Mujahid Robbani Sholahudin", 
			"email"=>"mujahidrobbanisholahudin@gmail.com", 
			"username"=>"mujahidrs", 
			"password"=>Hash::make("mujahidrs"),
			"role_id"=>$volunteer_role->id,
			"email_verified_at"=>date("Y-m-d H:i:s"),
			"gender"=>"male",
			"phone"=>"6289653132158",
			"class_id"=>9,
		]);

		$treasurer_role = Role::create(["name"=>"Treasurer"]);

        $treasurer = User::create([
			"name"=>"Bendahara", 
			"email"=>"bendahara@gmail.com", 
			"username"=>"bendahara", 
			"password"=>Hash::make("ybpmmd123"),
			"role_id"=>$treasurer_role->id,
			"email_verified_at"=>date("Y-m-d H:i:s"),
			"gender"=>"male",
			"phone"=>null,
			"class_id"=>null,
		]);

		Donation::create([
			"name" => "Zakat Fitrah",
	        "sort_number" => 1,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Fidyah",
	        "sort_number" => 2,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Zakat Maal",
	        "sort_number" => 3,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Zakat Profesi/Penghasilan",
	        "sort_number" => 4,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Infak Sedekah",
	        "sort_number" => 5,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Hadiah Guru Ngaji",
	        "sort_number" => 6,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Beasiswa Pemimpin Muda",
	        "sort_number" => 7,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Infak untuk Yatim",
	        "sort_number" => 8,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Infak Dunia Islam",
	        "sort_number" => 9,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Wakaf Rumah Quran",
	        "sort_number" => 10,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Wakaf Perguruan Tinggi Islam",
	        "sort_number" => 11,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Infak Program Muda Qurani",
	        "sort_number" => 12,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Siaga Covid-19",
	        "sort_number" => 13,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Pemberdayaan Ekonomi Muda",
	        "sort_number" => 14,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Bantuan Kesehatan Masyarakat",
	        "sort_number" => 15,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Sedekah Quran Braille",
	        "sort_number" => 16,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Wakaf Uang (Produktif)",
	        "sort_number" => 17,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Infak Program Tahfizh",
	        "sort_number" => 18,
	        "status" => "active",
		]);

		Donation::create([
			"name" => "Siaga Bencana Nasional",
	        "sort_number" => 19,
	        "status" => "active",
		]);

		$before = Period::create([
			"name" => "Before Ramadhan 1442 H",
			"start_date" => date('2020-05-24 00:00'),
			"end_date" => date('2021-04-13 06:00'),
			"total_target" => 0,
		]);

		Target::create(["period_id" => $before->id, "level_id" => 1, "target" => 0]);
		Target::create(["period_id" => $before->id, "level_id" => 2, "target" => 0]);
		Target::create(["period_id" => $before->id, "level_id" => 3, "target" => 0]);
		Target::create(["period_id" => $before->id, "level_id" => 4, "target" => 0]);
		Target::create(["period_id" => $before->id, "level_id" => 5, "target" => 0]);

		$ramadhan = Period::create([
			"name" => "Ramadhan 1442 H / 2021 M",
			"start_date" => date('2021-04-13 06:00'),
			"end_date" => date('2021-05-12 18:00'),
			"total_target" => 100000000,
		]);

		Target::create(["period_id" => $ramadhan->id, "level_id" => 1, "target" => 0]);
		Target::create(["period_id" => $ramadhan->id, "level_id" => 2, "target" => 0]);
		Target::create(["period_id" => $ramadhan->id, "level_id" => 3, "target" => 7000000]);
		Target::create(["period_id" => $ramadhan->id, "level_id" => 4, "target" => 9500000]);
		Target::create(["period_id" => $ramadhan->id, "level_id" => 5, "target" => 12000000]);
    }
}
